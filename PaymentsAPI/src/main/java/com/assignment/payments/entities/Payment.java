package com.assignment.payments.entities;

import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document
public class Payment {
    private int _id;
    private Date date;
    private String type;
    private double amount;

    public Payment() {
    }

    public Payment(int _id, Date date, String type, double amount) {
        this._id = _id;
        this.date = date;
        this.type = type;
        this.amount = amount;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
}
