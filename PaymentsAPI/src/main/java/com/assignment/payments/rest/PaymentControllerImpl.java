package com.assignment.payments.rest;

import com.assignment.payments.entities.Payment;
import com.assignment.payments.service.PaymentService;
import org.apache.coyote.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RestController
@RequestMapping("/api")
public class PaymentControllerImpl implements PaymentController {
    @Autowired
    private PaymentService service;

    @Override
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public HttpStatus getApiStatus() {
        return HttpStatus.OK;
    }

    @Override
    @RequestMapping(value = "/count", method = RequestMethod.GET)
    public long findTotal() {
        return service.count();
    }

    @Override
    @RequestMapping(value = "/find/{id}", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<Payment> findPayment(@PathVariable("id") int id) {
          Payment payment;
        if (id < 0) {
            return new ResponseEntity<Payment>(HttpStatus.BAD_REQUEST);
        } else {
            payment=service.findPayment(id);
            return new ResponseEntity<Payment>(payment,HttpStatus.OK);
        }
    }


    @Override
    @RequestMapping(value="/find/{type}",method=RequestMethod.GET,produces = "application/json")
    public ResponseEntity<List<Payment>> GetAll(@PathVariable("type") String type) {
        if(type!=null){
            List<Payment> payments = service.findPaymentByType(type);
            return new ResponseEntity<List<Payment>>(payments, HttpStatus.OK);
        }
        else{
            return new ResponseEntity<List<Payment>>(HttpStatus.BAD_REQUEST);
        }
    }
    @Override
    @RequestMapping(value="/save" ,method=RequestMethod.POST)
    public void Save(Payment payment) {
        service.SavePayment(payment);
    }
}
