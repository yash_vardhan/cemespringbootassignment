package com.assignment.payments.rest;

import com.assignment.payments.entities.Payment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

public interface PaymentController {
    @RequestMapping(value = "/",method=RequestMethod.GET)
    HttpStatus getApiStatus();
    @RequestMapping(value="/count",method= RequestMethod.GET)
    long findTotal();
    @RequestMapping(value = "/find/{id}",method=RequestMethod.GET)
   ResponseEntity<Payment> findPayment(@PathVariable("id") int id);
    @RequestMapping(value="find/{type}",method=RequestMethod.GET)
    ResponseEntity<List<Payment>> GetAll(@PathVariable("type") String type);
    @RequestMapping(value="/save" ,method=RequestMethod.POST)
    void Save(Payment payment);
}
