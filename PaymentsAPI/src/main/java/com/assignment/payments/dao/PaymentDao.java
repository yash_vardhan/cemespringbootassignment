package com.assignment.payments.dao;

import com.assignment.payments.entities.Payment;

import java.util.List;

public interface PaymentDao {
     long rowcount();
     Payment findByid(int id);
     List<Payment> findByType(String type);
     void save(Payment payment);
}
