package com.assignment.payments.dao;

import com.assignment.payments.entities.Payment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.*;
@Repository
public class PaymentDaoImpl implements PaymentDao {
    @Autowired
    MongoTemplate tmplt;

    @Override
    public long rowcount() {
        Query query = new Query();
        long count = tmplt.count(query, Payment.class);
        return count;
    }

    @Override
    public Payment findByid(int id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(id));
        Payment payment = tmplt.findOne(query, Payment.class);
        return payment;
    }

    @Override
    public List<Payment> findByType(String type) {
        Query query=new Query();
        query.addCriteria((Criteria.where("type").is(type)));
        List<Payment> payments = new ArrayList<Payment>();{
            payments=tmplt.find(query,Payment.class);
        }
        return  payments;
    }

    @Override
    public void save(Payment payment) {

        tmplt.save(payment);

    }
}

