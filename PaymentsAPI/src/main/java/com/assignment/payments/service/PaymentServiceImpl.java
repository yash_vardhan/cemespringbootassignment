package com.assignment.payments.service;

import com.assignment.payments.dao.PaymentDao;
import com.assignment.payments.entities.Payment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.InvalidDataAccessResourceUsageException;
import org.springframework.stereotype.Service;

import java.security.InvalidParameterException;
import java.util.List;
@Service
public class PaymentServiceImpl implements PaymentService{

    @Autowired
    private PaymentDao paymentdao;

    @Override
    public long count() {
        long count=paymentdao.rowcount();
        return count;
    }

    @Override
    public Payment findPayment(int id) {
        Payment payment= paymentdao.findByid(id);
        if(payment!=null) {
            return payment;
        }
        else{
            throw new InvalidDataAccessResourceUsageException("Could not find DB resource");
        }

    }

    @Override
    public List<Payment> findPaymentByType(String type) {
        List<Payment> payments= paymentdao.findByType(type);
        if(payments!=null){
            return payments;
        }
        else{
            throw new InvalidDataAccessResourceUsageException("Could not find the DB Resource");
        }
    }
    @Override
    public void SavePayment(Payment payment) {
           paymentdao.save(payment);
    }
}

