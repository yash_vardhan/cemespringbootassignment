package com.assignment.payments.service;

import com.assignment.payments.entities.Payment;

import java.util.List;

public interface PaymentService {
    long count();
    Payment findPayment(int id);
    List<Payment> findPaymentByType(String type);
    void SavePayment(Payment payment);
}
